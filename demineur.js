var dim = 10;
var grille;

function Case(){
	this.nbrBombVoisines = 0;
	this.bomb = false;
	this.cache = true;
	this.drapeaux = false;
	this.boum = false;
}

function initAffichage(){
var i,j;
	var table = document.createElement("table");
	for(i = 0;i <dim ; i++){
	var ligne = document.createElement("tr");
		for(j=0 ; j<dim ; j++){
			var caze = document.createElement("td");
			ligne.appendChild(caze);
		}
	table.appendChild(ligne);
	}
	document.body.appendChild(table);
}

function initGrille(){
 var i,j;
 grille = [];

 for (i=0;i<dim;i++){
	grille[i]=[];
	for (j=0;j<dim;j++)
		grille[i][j] = new Case();
	}
	poserBombe();
	compteBombe();
}

function affichage(){
	var i,j;
	var cazes = document.getElementsByTagName("td"); // r�cup�re toutes les "td"
	for(i=0; i<dim;i++){
		for(j=0; j<dim;j++){
			if(grille[i][j].cache){
				if(grille[i][j].drapeaux){
					cazes[dim*i+j].setAttribute("class","drapeaux");
				}
				else 
					cazes[dim*i+j].setAttribute("class","cache"); // calcule pour faire correspondre des tableaux de 1 � 2 dimensions
			}
			else{
				if(grille[i][j].bomb == true){
					cazes[dim*i+j].setAttribute("class","bombe");
					}
				else if(grille[i][j].nbrBombVoisines == 1){
					cazes[dim*i+j].setAttribute("class","un");
				}
				else if(grille[i][j].nbrBombVoisines == 2){
					cazes[dim*i+j].setAttribute("class","deux");
				}
				else if(grille[i][j].nbrBombVoisines == 3){
					cazes[dim*i+j].setAttribute("class","trois");
				}
				else if(grille[i][j].nbrBombVoisines == 4){
					cazes[dim*i+j].setAttribute("class","quatre");
				}
				else if(grille[i][j].nbrBombVoisines == 5){
					cazes[dim*i+j].setAttribute("class","cinq");
				}
			}
				
				// else 
				// cazes[dim*i+j].setAttribute("class","vide");
				
			// if(grille[i][j].bomb == true){
				// cazes[dim*i+j].setAttribute("class","bombe");
			// }
			// else 
				// cazes[dim*i+j].setAttribute("class","cache");
				
	
		}
	}
}

function poserBombe(){
	var i,j;
	for(i=0;i<dim;i++){
		for(j=0;j<dim;j++){
			if(Math.random()<0.1)
				grille[i][j].bomb = true;
		}
	}
}

function compteBombe(){
	var i,j;
	var nbBombe;
	for(i=0;i<dim;i++){
		for(j=0;j<dim;j++){
			nbBombe = 0;
			if( (i>0) && (j>0) && (grille[i-1][j-1].bomb)){
				nbBombe++;
			}
			
			if((i>0)&& (grille[i-1][j].bomb)){
				nbBombe++;
			}
			if((i>0)&& (grille[i-1][j].bomb)){
				nbBombe++;
			}
			
			if((i>0) && (j<dim-1)&& (grille[i-1][j+1].bomb)){
				nbBombe++;
			}
			
			if((j<dim-1) && (grille[i][j+1].bomb)){
				nbBombe++;
			}
			
			if( (i<dim-1) && (j<dim-1) && (grille[i+1][j+1].bomb)){
				nbBombe++;
			}
			
			if( (i<dim-1) && (grille[i+1][j].bomb)){
				nbBombe++;
			}
			
			if( (i<dim-1) && (j<0) && (grille[i+1][j-1].bomb)){
				nbBombe++;
			}
			
			if( (j<0) && (grille[i+1][j-1].bomb)){
				nbBombe++;
			}
			
			grille[i][j].nbrBombVoisines = nbBombe;
			
		}
	}
}

function dectection(event){
var cases = document.getElementsByTagName("td");
var i,ligne,colonne;

	for(i=0;i<cases.length;i++){
		if(cases[i] == event.target){
			ligne = parseInt(i/dim);
			colonne = i%dim;
			switch(event.button){
				case 0:
					if(!grille[ligne][colonne].drapeaux)
						decouvrir(ligne,colonne);
					break;
				case 1:
					rien(event);
					break;
				case 2:
					marque(ligne,colonne);
					break;
			}
			affichage();
		}
	}
}

function decouvrir(ligne,colonne){
if(grille[ligne][colonne].cache){
	grille[ligne][colonne].cache = false;
	
	if(grille[ligne][colonne].bomb)
		perdu();
		if(grille[ligne][colonne].nbrBombVoisines == 0){
			if((i>0) && (j>0) && (grille[ligne-1][colonne-1].cache))
				decouvrir(ligne-1,colonne);
			if((i>0) &&  (grille[ligne-1][colonne+1].cache))
				decouvrir(ligne-1,colonne+1);	
			if((j>0) &&  (grille[ligne-1][colonne+1].cache))
				decouvrir(ligne-1,colonne+1);	
			if((j<dim-1) &&  (grille[ligne][colonne+1].cache))
				decouvrir(ligne,colonne+1);
			if((i<dim-1) && (j<dim-1) && (grille[ligne+1][colonne+1].cache))
				decouvrir(ligne+1,colonne+1);
			if((i<dim-1) && (grille[ligne+1][colonne].cache))
				decouvrir(ligne+1,colonne);
			if((i<dim-1) (j>0) && (grille[ligne+1][colonne-1].cache))
				decouvrir(ligne+1,colonne-1);
			if((j>0) && (grille[ligne][colonne-1].cache))
				decouvrir(ligne,colonne-1);			
		}
	}
}

function marque(ligne,colonne){
if(grille[ligne][colonne].cache)
	grille[ligne][colonne].drapeaux = !grille[ligne][colonne].drapeaux;
}

function perdu(){
	alert("BOUMMMMMM");

}
	

function rien(event){
	event.preventDefault();
}

function init(){
	initGrille();
	initAffichage();
	affichage();
	window.addEventListener("contextmenu",rien,false);
	window.addEventListener("click",dectection,false);
	
}

window.onload = init;